/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.techbrothers.techsolutions.springbootfreemarker.controllers;

import com.techbrothers.techsolutions.springbootfreemarker.domain.User;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author chiru
 */
@Controller
@SessionAttributes({"user"})
public class UserController {
    
    @ModelAttribute("user")
    public User user(){
        return new User();
    }
    
    @Value("${applicationuser}")
    private String applicationuser;
    
    @GetMapping("/user")
    public String showUserInfo(Model model){
        if(!model.asMap().containsKey("user")){
          model.addAttribute("user", new User());
        }
        model.addAttribute("applicationuser", applicationuser);
        return "user";
    } 
    
    @PostMapping(value = "/user")
    public String createUser(@Valid User user, BindingResult bindingResult, 
            RedirectAttributes redirectAttributes){
        if(bindingResult.hasFieldErrors()){
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", bindingResult);
            return "redirect:/user";
        }
        System.out.println("User inform" + user);
        redirectAttributes.addFlashAttribute("user", user);
        return "redirect:/display";
    }
}
